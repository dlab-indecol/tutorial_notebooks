****************************************
Collection of Tutorial Notebooks of IEDL
****************************************

Here we gather all tutorial notebooks developed 
by the 
`Industrial Ecology Digital Laboratory <http://iedl.no/>`_.

You can run the notebooks interactively without installing by clicking on the binder button below:

.. image:: https://mybinder.org/badge_logo.svg 
     :target: https://mybinder.org/v2/gl/dlab-indecol%2Ftutorial_notebooks/master?filepath=.%2Fnotebooks

Contribution
============

We are open for any contribution under an open licence.
We recommend the 
`CC-BY 4.0 <https://creativecommons.org/licenses/by/4.0/>`_
licence for notebooks.
